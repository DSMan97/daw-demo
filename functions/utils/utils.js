const CryptoJS = require('crypto-js');
const admin = require('firebase-admin');
const request = require('request-promise');
const db_admin = require('../utils/firebase-utils').db;
const requestIp = require('request-ip');


const IGNORE_TOKEN_REQUEST = [];

const IGNORE_EMAIL_VALIDATE_REQUEST = [];

function isRequestInArray(request, array){
  return array.find(element => request.startsWith(element));
}

// Encrypt in HEX to be URL friendly
function encrypt(string) {
  const b64 = CryptoJS.AES.encrypt(string, SECRET_CRYPTO_KEY).toString();
  const e64 = CryptoJS.enc.Base64.parse(b64);
  const eHex = e64.toString(CryptoJS.enc.Hex);
  return eHex;
}

// Decrypt from HEX to be URL friendly
function decrypt(encryptedString) {

  const reb64 = CryptoJS.enc.Hex.parse(encryptedString);
  const bytes = reb64.toString(CryptoJS.enc.Base64);
  const decrypt = CryptoJS.AES.decrypt(bytes, SECRET_CRYPTO_KEY);
  const plain = decrypt.toString(CryptoJS.enc.Utf8);

  return plain;
}

//Authenticated API
const validateFirebaseIdTokenRequest = async (req, res, next) => {
  const ip = requestIp.getClientIp(req);
  console.log('Called From IP Address: ', ip);
  console.log('Called From City Address: ', req.headers['x-appengine-city']);
  console.log('Google Contacs Token: ', req.headers['googletoken']);
  console.log('>>>>>> Petición comenzada a:', req.path);
  let idToken;
  if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
    console.log('Found "Authorization" header');

    // Read the ID Token from the Authorization header.
    idToken = req.headers.authorization.split('Bearer ')[1];
    // console.log("==============token===========", idToken);
  } else if(req.cookies) {
    console.log('Found "__session" cookie');
    // Read the ID Token from cookie.
    idToken = req.cookies.__session;
  } else {
    // No cookie
    res.status(403).send({
      error: true,
      message: 'UNAUTHORIZED'
    });
    return;
  }
  try {
    const decodedIdToken = await admin.auth().verifyIdToken(idToken, true);
  //  console.log('=======decode token=======', decodedIdToken);
    //console.log('ID Token correctly decoded');

    // Peticiones de las que no se espera tener el email verificado.
    if (!decodedIdToken.email_verified) {
      if (isRequestInArray(req.path, IGNORE_EMAIL_VALIDATE_REQUEST)) {
        console.log('Ignorar validateFirebaseIdToken en petición:', req.path);
        req.user = decodedIdToken;

        next();
        return;
      }

      res.status(403).send({
        error: true,
        message: 'EMAIL_NOT_VERIFIED'
      });
      return;
    }

    req.user = decodedIdToken;

    next();
    return;
  } catch (error) {
    console.error('Error while verifying Firebase ID token:', error);
    res.status(403).send({
      error: true,
      message: 'UNAUTHORIZED'
    });
    return;
  }
};

const validateFirebaseIdAdminTokenRequest = async (req, res, next) => {
  console.log('>>>>>> Petición de tipo Admin <<<<<<<<');
  const ip = requestIp.getClientIp(req);
  console.log('Called From IP Address: ', ip);
  let city = 'desconocida';
  if(req.headers['x-appengine-city'] !== undefined){
    city = req.headers['x-appengine-city'];
  }
  console.log('Called From City Address: ', city);
  console.log('>>>>>> Petición comenzada a:', req.path);
  // console.log('Authorization: ',req.headers.authorization);
  // algunas rutas no deben de estar autenticadas con token:
  if (isRequestInArray(req.path, IGNORE_TOKEN_REQUEST)) {
    console.log('Ignorar validateFirebaseIdToken en petición:', req.path);
    next();
    return;
  }
  let idToken;
  if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
    console.log('Found "Authorization" header');

    // Read the ID Token from the Authorization header.
    idToken = req.headers.authorization.split('Bearer ')[1];
   // console.log("==============token===========", idToken);
  } else if(req.cookies) {
    console.log('Found "__session" cookie');
    // Read the ID Token from cookie.
    idToken = req.cookies.__session;
  } else {
    // No cookie
    res.status(403).send({
      error: true,
      message: 'UNAUTHORIZED'
    });
    return;
  }
  try {
    const decodedIdToken = await admin.auth().verifyIdToken(idToken, true);
   // console.log('=======decode token=======', decodedIdToken);
    //console.log('ID Token correctly decoded');

    // Peticiones de las que no se espera tener el email verificado.
    if (!decodedIdToken.email_verified) {
      if (isRequestInArray(req.path, IGNORE_EMAIL_VALIDATE_REQUEST)) {
        console.log('Ignorar validateFirebaseIdToken en petición:', req.path);
        req.user = decodedIdToken;
        next();
        return;
      }

      res.status(403).send({
        error: true,
        message: 'EMAIL_NOT_VERIFIED'
      });
      return;
    }
    req.user = decodedIdToken;
    const userUid = req.user.user_id;
    if(! await isAdmin(userUid)){
      res.status(403).send({
        error: true,
        message: 'UNAUTHORIZED'
      });
      return;
    }



    next();
    return;
  } catch (error) {
    console.error('Error while verifying Firebase ID token:', error);
    res.status(403).send({
      error: true,
      message: 'UNAUTHORIZED'
    });
    return;
  }
};

//Authenticated API
const  validateFirebaseIdToken = async (admin, req) => {

 /* if (req.hostname === 'localhost') {
    return {
      "email": "guille@pleg.es",
      "uid": "qBbvooXrf2Pjq93m2jSr8DnJDVw2",
      "email_verified": false
    }

  }*/

  if ((!req.headers.authorization || !req.headers.authorization.startsWith('Bearer ')) &&
      !(req.cookies && req.cookies.__session)) {
    console.error(
      'No Firebase ID token was passed as a Bearer token in the Authorization header.',
      'Make sure you authorize your request by providing the following HTTP header:',
      'Authorization: Bearer <Firebase ID Token>',
      'or by passing a "__session" cookie.'
    );
    return {
      error: true,
      message: 'UNAUTHORIZED'
    }; // Unauthorized
  }

  let idToken;
  if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
    // Read the ID Token from the Authorization header.
    idToken = req.headers.authorization.split('Bearer ')[1];
  } else if(req.cookies) {
    // Read the ID Token from cookie.
    idToken = req.cookies.__session;
  } else {
    // No cookie
    return {
      error: true,
      message: 'UNAUTHORIZED'
    }; // Unauthorized
  }
  try {
    const decodedIdToken = await admin.auth().verifyIdToken(idToken);
    // console.log('ID Token correctly decoded');

    if (!decodedIdToken.email_verified) {
      return {
        error: true,
        message: 'EMAIL_NOT_VERIFIED'
      }; // Unauthorized
    }

    return decodedIdToken;
  } catch (error) {
    console.error('Error while verifying Firebase ID token:', error);
    return {
      error: true,
      message: 'UNAUTHORIZED'
    }; // Unauthorized
  }
};

// Valida si el usuario es administrador o no
async function isAdmin(user_id) {
  let admin = false;
  const myUser = await db_admin.collection("users").doc(user_id).get();
  if(myUser){
    admin = myUser.data().role === 'admin';
  }else{
    console.log("usuario no encontrado!!", user_id)
  }
  return admin
}


const getToken = async (url) =>{
    const receivingServiceURL = url;
    // See https://cloud.google.com/compute/docs/instances/verifying-instance-identity#request_signature
    const metadataServerTokenURL = 'http://metadata/computeMetadata/v1/instance/service-accounts/default/identity?audience=';
    const tokenRequestOptions = {
        uri: metadataServerTokenURL + receivingServiceURL,
        headers: {
            'Metadata-Flavor': 'Google'
        }
    };
    // Fetch the token, then provide the token in the request to the receiving service
    return request(tokenRequestOptions);
}

exports.validateFirebaseIdTokenRequest = validateFirebaseIdTokenRequest;
exports.validateFirebaseIdToken = validateFirebaseIdToken;
exports.validateFirebaseIdAdminTokenRequest = validateFirebaseIdAdminTokenRequest;
exports.encrypt = encrypt;
exports.decrypt = decrypt;
exports.isAdmin = isAdmin;
exports.getToken = getToken;
