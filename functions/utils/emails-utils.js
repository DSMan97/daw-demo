import fs from 'fs';
import nodemailer from 'nodemailer';
import * as path from "path";
const configSMTP = require("../assets/configServerSMTP/SMTP-config.ts");

const rootPath = path.resolve(__dirname, '..');
const emailTemplate = path.join(rootPath,'/assets/email-templates/notifications/email-template.html');

/**
 * Check https://nodemailer.com/message/ for more options
 */
const getTransport = (data) => {
    return nodemailer.createTransport({
        service: data.service,
        host: data.host, // PROD
        port: data.port, // PROD
        secureConnection: data.secure, // PROD
        auth: data.auth, // PROD
        debug: false,
        logger: false,
    });
};

export async function getEmailHtml(variables) {
    return new Promise((resolve, reject) => {
        formatEmail(emailTemplate, variables, (error, data) => {
            if (error) {
                console.error('Error reading HTML:', error);
                reject(error);
            } else {
                resolve(data);
            }
        });
    });
}

function formatEmail(filePath, variables, callback) {
    fs.readFile(filePath, 'utf8', (err, template) => {
        if (err) {
            console.error(err);
            callback(err, null);
            return;
        }

        // Replace placeholders with variable values
        for (const variable in variables) {
            if (variables.hasOwnProperty(variable)) {
                const value = variables[variable];
                const placeholder = `{{${variable}}}`;
                template = template.replace(new RegExp(placeholder, 'g'), value);
            }
        }

        // Return the modified HTML content
        callback(null, template);
    });
}

export function convertToTitle(str) {
    const words = str.split('-'); // Split the string into words separated by hyphens
    const title = words.map(word => word.charAt(0).toUpperCase() + word.slice(1)); // Capitalize the first letter of each word
    return title.join(' '); // Join the words with spaces
}

export function shortenString(str) {
    if (str.length > 18) {
        return str.substring(0, 14) + "...";
    }
    return str;
}

export async function sendEmail(from, bcc, subject, html, response, res) {
    const transporter = getTransport(configSMTP);

    const mailOptions = {
        from,
        bcc,
        html,
        subject
    };

    await transporter.sendMail(mailOptions, (error) => {
        if (error) {
            console.warn(`ERROR SENDING EMAIL TO ${mailOptions.bcc}:`);
            console.warn(error);
            response = {
                success: false,
                statusCode: 500,
                message: `ERROR SENDING EMAIL TO ${mailOptions.bcc}`,
                error: error
            };
            res.status(response.statusCode).json(response);
        } else {
            console.log(`✔ ${mailOptions.bcc} email notified.`);
            response = {
                success: true,
                statusCode: 200,
                message: `Email sent successfully to ${mailOptions.bcc}`,
            };
            res.status(response.statusCode).json(response);
        }
    });
}
