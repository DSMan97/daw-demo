const checkChanges = function (oldValue, newValue, fieldsToUpdate) {
    let result = false;
    for (let index = 0; index < fieldsToUpdate.length; index++) {
        const myField = fieldsToUpdate[index];
        if (oldValue[myField] !== newValue[myField]) {
            result = true;
            break;
        }
    }
    return result;
};
exports.checkChanges = checkChanges;
